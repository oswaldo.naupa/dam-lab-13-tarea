import 'dart:convert';
import 'package:flutter/services.dart' show rootBundle;

class _PeliculaProvider {
  List<dynamic> opciones = [];

  Future<List<dynamic>> cargarDataPelicula() async {
    final resp = await rootBundle.loadString('data/pelicula.json');

    Map dataMap = json.decode(resp);


    //print(datMap);

    opciones = dataMap['peliculas'];

    return opciones;
  }
  Future<List<dynamic>> cargarDataPeliculaProximos() async {
    final resp = await rootBundle.loadString('data/pelicula.json');

    Map dataMap = json.decode(resp);


    //print(datMap);

    opciones = dataMap['proximos'];

    return opciones;
  }
}

final menuProvider = new _PeliculaProvider();