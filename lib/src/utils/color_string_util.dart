import 'package:flutter/material.dart';

Color getColorByEvent(String nombrecolor) {
   if (nombrecolor == "red") return Colors.red;
   if (nombrecolor == "blue") return Colors.blue;
   if (nombrecolor == "green") return Colors.green;
   if (nombrecolor == "yellow") return Colors.yellow;
   if (nombrecolor == "orange") return Colors.orange;
   if (nombrecolor == "indigo") return Colors.indigo;
}