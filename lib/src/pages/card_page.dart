import 'package:flutter/material.dart';

class CardPage extends StatelessWidget {
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.indigo[100],
      appBar: AppBar(
         actions: <Widget>[
            Image.network(
                'https://tspro.mktoolbox.net/wp-content/uploads/2019/03/logotipo-TECSUP-trans-02-02.png',
                  )
          ],
        title: Text('Card',style:TextStyle(fontFamily: 'Waltograph',fontSize: 28)),
        backgroundColor: Color(0xFF3F51B5),
      ),
      body: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Card(
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    Image.network(
                    'https://e.rpp-noticias.io/normal/2020/06/29/192219_963332.jpg',
                      ),
                    const ListTile(
                      leading: Icon(Icons.album),
                      title: Text('Reactivación económica'),
                      subtitle: Text('Los aerolíneas podrían reiniciar los vuelos nacionales a partir de la primera quincena de julio, según reveló la semana pasada el titular del Ministerio de Transportes y Comunicaciones (Mincetur), Carlos Lozada.',style:TextStyle(color:Color(0xFF3F51B5))),
                    ),
                    ButtonBar(
                      children: <Widget>[
                        FlatButton(
                          child: const Text('Ver Mas'),
                          onPressed: () {/* ... */},
                        ),
                        FlatButton(
                          child: const Text('Volver'),
                          onPressed: () {/* ... */},
                        ),
                      ],
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
       floatingActionButton: FloatingActionButton(
          child: Icon(Icons.arrow_back_ios),
          backgroundColor: Color(0xFFFF5722),
          onPressed: () => {
              Navigator.of(context).pop('Ok')
          },
        ),
    );
  }
}