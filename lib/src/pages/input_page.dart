import 'package:flutter/material.dart';

class InputPage extends StatelessWidget {
   final _formKey = GlobalKey<FormState>();
   String _nombre="";
   String _contrasena="";

  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.indigo[100],
      appBar: AppBar(
         actions: <Widget>[
            Image.network(
                'https://tspro.mktoolbox.net/wp-content/uploads/2019/03/logotipo-TECSUP-trans-02-02.png',
                  )
          ],
        title: Text('Input',style:TextStyle(fontFamily: 'Waltograph',fontSize: 28)),
        backgroundColor: Color(0xFF3F51B5),
      ),
      body: Center(
          child:Padding(
          padding: const EdgeInsets.all(25.0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.all(16.0),
                child:
                  Text('Ingrese sus Datos:',style:TextStyle(fontFamily: 'Waltograph', color: Colors.indigo[900],fontSize: 28)),
              ),
              Card(child: 
               Form(
                  key: _formKey,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      TextFormField(
                        decoration: const InputDecoration(
                          hintText: 'Ingrese su usuario',
                        ),
                        validator: (value) {
                          if (value.isEmpty) {
                            return 'El nombre no puede ser vacia';
                          }
                          return null;
                        },
                        onSaved: (value) => _nombre = value,
                      ),
                      TextFormField(
                        decoration: const InputDecoration(
                          hintText: 'Ingrese su contraseña',
                        ),
                        validator: (value) {
                          if (value.isEmpty) {
                            return 'El contraseña no puede ser vacia';
                          }
                          return null;
                        },
                        onSaved: (value) => _contrasena = value,
                        obscureText: true,
                      ),
                      Padding(
                        padding: const EdgeInsets.all(16.0),
                        child: RaisedButton(
                          onPressed: () {
                            // Validate will return true if the form is valid, or false if
                            // the form is invalid.
                            if (_formKey.currentState.validate()) {
                              // Process data.
                                _formKey.currentState.save();
                                showDialog(
                                  context: context,
                                  barrierDismissible: false,
                                  builder: (context) => AlertDialog(
                                    title: Text('Mensaje', style: TextStyle(color: Colors.indigo[900],fontWeight: FontWeight.bold)),
                                    content: Text('Sus datos son:\n-nombre: $_nombre \n-contraseña: $_contrasena'),
                                    actions: <Widget>[
                                      FlatButton(
                                        child: Text('Ok'),
                                        onPressed: (){
                                          Navigator.of(context).pop('Ok');
                                        },
                                      )
                                    ],
                                  )
                                );
                            }
                          },
                          child: Text('Enviar'),
                        ),
                      ),
                     //Text('Datos recibidos', style: TextStyle(fontSize: 25)),
                     //Text('$_nombreinput', style: TextStyle(fontSize: 50, color: Colors.indigo[900])),
                    ],
                  ),
                )
              ),
            ],
          ),
        ),
      ),
       floatingActionButton: FloatingActionButton(
          child: Icon(Icons.arrow_back_ios),
          backgroundColor: Color(0xFFFF5722),
          onPressed: () => {
              Navigator.of(context).pop('Ok')
          },
        ),
    );
  }
}