import 'package:flutter/material.dart';
import 'package:avataaar_image/avataaar_image.dart';
//import 'package:getflutter/getflutter.dart';

class AvatarPage extends StatelessWidget {
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.indigo[100],
      appBar: AppBar(
        actions: <Widget>[
            Image.network(
                'https://tspro.mktoolbox.net/wp-content/uploads/2019/03/logotipo-TECSUP-trans-02-02.png',
                  )
          ],
        title: Text('Avatars',style:TextStyle(fontFamily: 'Waltograph',fontSize: 28)),
        backgroundColor: Color(0xFF3F51B5),
      ),
       body: Center(
          child:  
            ExampleBody(),
       ),
       floatingActionButton: FloatingActionButton(
           child: Icon(Icons.arrow_back_ios),
          backgroundColor: Color(0xFFFF5722),
          onPressed: () => {
              Navigator.of(context).pop('Ok')
          },
        ),
    );
  }
}
class ExampleBody extends StatefulWidget {
  @override
  _ExampleBodyState createState() => _ExampleBodyState();
}

class _ExampleBodyState extends State<ExampleBody> {
  Avataaar _avatar;

  @override
  void initState() {
    super.initState();
    _randomizeAvatar();
  }
void _randomizeAvatar() => _avatar = Avataaar.random();

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.all(16.0),
      child: Column(children: [
        Text('Generador de Avatars',style:TextStyle(fontFamily: 'Waltograph',fontSize: 28)),
        Expanded(
          child: Center(
            child: AvataaarImage(
              avatar: _avatar,
              errorImage: Icon(Icons.error),
              placeholder: CircularProgressIndicator(),
              width: 128.0,
            ),
          ),
        ),
        IconButton(
          iconSize: 48.0,
          icon: Icon(Icons.refresh),
          onPressed: () => setState(_randomizeAvatar),
        ),
      ]),
    );
  }
}
