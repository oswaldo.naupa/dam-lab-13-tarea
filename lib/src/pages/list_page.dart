import 'package:componentes/src/providers/pelicula_provider.dart';
import 'package:componentes/src/utils/color_string_util.dart';
import 'package:flutter/material.dart';

class ListPage extends StatelessWidget {
  
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.indigo[100],
      appBar: AppBar(
         actions: <Widget>[
            Image.network(
                'https://tspro.mktoolbox.net/wp-content/uploads/2019/03/logotipo-TECSUP-trans-02-02.png',
                  )
          ],
        title: Text('Lista',style:TextStyle(fontFamily: 'Waltograph',fontSize: 28)),
        backgroundColor: Color(0xFF3F51B5),
      ),
       body:  Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Text('Mejores Peliculas',style:TextStyle(fontFamily: 'Waltograph',fontSize: 28)),
                      _lista(),
                      Text('Proximos Extrenos',style:TextStyle(fontFamily: 'Waltograph',fontSize: 28)),
                      _listaProxima(),
                      
                    ],
       ),
       floatingActionButton: FloatingActionButton(
          child: Icon(Icons.arrow_back_ios),
          backgroundColor: Color(0xFFFF5722),
          onPressed: () => {
              Navigator.of(context).pop('Ok')
          },
        ),
    );
  }
}

Widget _lista(){
    return FutureBuilder(
      future: menuProvider.cargarDataPelicula(),
      initialData: [],
      builder: (context, AsyncSnapshot<List<dynamic>> snapshot){
        return 
        Center(
          child: Container(
          margin: EdgeInsets.symmetric(vertical: 20.0),
          height: 200.0,
          child:  ListView(
          scrollDirection: Axis.horizontal,
          children: _listaItems(snapshot.data, context)
          ),
          ),
        );
      },
    );
  }
List<Widget> _listaItems(List<dynamic> data, context){
    final List<Widget> opciones = [];

    data.forEach((opt) {
      final widgetTemp = 
    
      Container(
                width: 250.0,
                color: getColorByEvent(opt['color']),
                child: Padding( 
                  padding: const EdgeInsets.all(15.0),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                    Image.network(
                    opt['ruta'],
                      fit:BoxFit.fill
                    ),
                    Text(opt['texto'],style:TextStyle(fontSize: 28)),
                    ],
                  ),
                ),
              );
               opciones..add(widgetTemp);
     });

     return opciones;
  }

  Widget _listaProxima(){
    return FutureBuilder(
      future: menuProvider.cargarDataPeliculaProximos(),
      initialData: [],
      builder: (context, AsyncSnapshot<List<dynamic>> snapshot){
        return 
        Center(
          child: Container(
          margin: EdgeInsets.symmetric(vertical: 20.0),
          height: 200.0,
          child:  ListView(
          scrollDirection: Axis.horizontal,
          children: _listaItemsProximos(snapshot.data, context)
          ),
          ),
        );
      },
    );
  }
  List<Widget> _listaItemsProximos(List<dynamic> data, context){
    final List<Widget> opciones = [];

    data.forEach((opt) {
      final widgetTemp = 
    
      Container(
                width: 250.0,
                color: getColorByEvent(opt['color']),
                child: Padding( 
                  padding: const EdgeInsets.all(15.0),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                    Image.network(
                    opt['ruta'],
                      fit:BoxFit.fill
                    ),
                    Text(opt['texto'],style:TextStyle(fontSize: 28)),
                    ],
                  ),
                ),
              );
               opciones..add(widgetTemp);
     });

     return opciones;
  }
